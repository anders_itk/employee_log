#/bin/bash
docker stop employee_api && docker rm employee_api
docker build -t employee_api -f ./docker/Dockerfile .
docker run -itd -p 6660:6660 --name employee_api employee_api
echo $(docker logs employee_api)
