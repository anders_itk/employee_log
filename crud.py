from sqlalchemy.orm import Session
import models

def log_user(db: Session, serial: str, status: str) -> models.UserAccess:
    db_access = models.UserAccess(serial=serial, status=status)
    db.add(db_access)
    db.commit()
    db.refresh(db_access)
    return db_access

def get_all_logs(db: Session):
    return db.query(models.UserAccess).all()

def truncate(db: Session) -> bool:
    """
    Delete all rows from table
    :param db: db session
    :return: boolean False if exception occurred, otherwise True.
    """
    try:
        db.execute(f"TRUNCATE TABLE {models.UserAccess.__tablename__}")
        db.commit()
    except:
        return False
    else:
        return True

