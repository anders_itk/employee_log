from typing import Optional
from fastapi import FastAPI, Depends, HTTPException, status
from sqlalchemy.orm import Session
import crud, models
from database import SessionLocal, engine
from fastapi.security import HTTPBasic, HTTPBasicCredentials
import secrets
security = HTTPBasic()
import os



models.Base.metadata.create_all(bind=engine)

# TODO consider using a tool for migrations https://fastapi.tiangolo.com/tutorial/sql-databases/?h=sql#create-a-dependency

app = FastAPI(version='0.1')

# Dependency
def get_db():
    #explanation = https://fastapi.tiangolo.com/tutorial/dependencies/dependencies-with-yield/#a-dependency-with-yield-and-try
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.post("/arrive/{serial_number}", status_code=200)
def log_arrive(serial_number: str, db: Session = Depends(get_db)):
    return crud.log_user(db=db, status='ARRIVE', serial=serial_number)

@app.post("/leave/{serial_number}", status_code=200)
def log_leave(serial_number: str, db: Session = Depends(get_db)):
    return crud.log_user(db=db, status='LEAVE', serial=serial_number)

@app.get("/all_entries", status_code=200)
def get_everything(db: Session = Depends(get_db)):
    all_logs = crud.get_all_logs(db=db)
    return all_logs

# POST, given credentials,
@app.post("/truncate", status_code=200)
def truncate(db: Session = Depends(get_db), credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = secrets.compare_digest(credentials.username, os.getenv('employee_log_username'))
    correct_password = secrets.compare_digest(credentials.password, os.getenv('employee_log_password'))
    if not (correct_username and correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )

    # https://mariadb.com/kb/en/truncate-table/ auto increment PK is reset.
    # all rows are dropped from the table
    if crud.truncate(db=db):
        return 'ok'
    else:
        raise HTTPException(status_code=500, detail="Exception while truncating.")

@app.get("/", status_code=200)
def server_status():
    return 200
