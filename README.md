# Overview
API build with [FastAPI](https://fastapi.tiangolo.com/)

## Requirements
Docker
Python 3.9

# Development & testing

## Database setup for testing
```
# docker volume create some-maria-data
# docker run -p 3306:3306 -v some-maria-data:/var/lib/mysql --name some-mariadb -e MARIADB_ROOT_PASSWORD=123456 -d mariadb
# docker exec -it some-mariadb bash
# § mysql -u root -p
CREATE DATABASE docker_employee_api;
CREATE USER 'docker_employee_logger'@'%' IDENTIFIED BY 'cat123';
GRANT ALL ON docker_employee_api.* TO 'docker_employee_logger'@'%';
FLUSH PRIVILEGES;
# validate: SHOW GRANTS FOR 'docker_employee_logger'@'%';
```
Start the api locally from terminal
`uvicorn main:app`

# Production
## Build & Run
`docker build -t employee_api -f ./docker/Dockerfile .`

`docker run -itd -p <docker_host_port>:6660 --name employee_api employee_api`

## API Endpoints
http://<machine_IP>:<docker_host_port>/docs

or

http://<machine_IP>:<docker_host_port>/redoc

