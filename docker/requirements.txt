uvicorn==0.14.0
starlette==0.14.2
fastapi==0.65.2
sqlalchemy==1.4.18
PyMySQL==1.0.2
