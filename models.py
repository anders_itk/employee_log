from sqlalchemy import Column, Integer, String, DateTime
from datetime import datetime
from database import Base

class UserAccess(Base):
    __tablename__ = "access"

    id = Column(Integer, primary_key=True, index=True)
    serial = Column(String(100), nullable=False)
    status = Column(String(100), nullable=False)
    entry_date = Column(DateTime, nullable=False, default=datetime.utcnow) # convert to summer/winter time at time of use
