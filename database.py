from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# running as docker container. Connecting to container IP
SQLALCHEMY_DATABASE_URL = "mysql+pymysql://docker_employee_logger:cat123@172.17.0.1:3306/docker_employee_api"

# running in terminal / Pycharm. Connecting to 0.0.0.0
#SQLALCHEMY_DATABASE_URL = "mysql+pymysql://docker_employee_logger:cat123@127.0.0.1:3306/docker_employee_api"


engine = create_engine(SQLALCHEMY_DATABASE_URL,
                       pool_pre_ping=True)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
